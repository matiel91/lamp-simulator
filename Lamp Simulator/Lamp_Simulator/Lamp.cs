﻿namespace Lamp_Simulator
{
    /// <summary>
    /// Create a lamp and assing Halogen object. 
    /// </summary>
    class Lamp
    {
        private Halogen _halogen;
        private bool _isWorking = false;
        private bool _isBroke = false;

        /// <summary>
        /// Assing new halogen to lamp
        /// </summary>
        /// <param name="halogen">Halogen</param>
        public Lamp(Halogen halogen)
        {
            _halogen = halogen;
        }
        public void SwitchOn()
        {
            _halogen.HalogenBurnedOut();
            if (_isWorking == true)
            {
                Communicates.AlreadySwitchedOn();
            }
            else if (_isBroke == true)
            {
                Communicates.IsBroken();
            }
            else if (_halogen.IsWorking == true && _isBroke == false)
            {
                Communicates.SwitchedOn();
                _isWorking = true;
            }
            else
            {
                Communicates.IsBroken();
                Communicates.HalogenRequired();
                _isBroke = true;
                _isWorking = false;
            }
        }

        public void SwitchOff()
        {
            if (_isBroke == true)
            {
                Communicates.IsBroken();
            }
            else if (_isWorking == false)
            {
                Communicates.AlreadySwitchedOff();
            }
            else
            {
                Communicates.SwitchedOff();
                _isWorking = false;
            }
        }
        public void replaceHalogen()
        {
            if (false == _isBroke)
            {
                Communicates.HalogenOperational();
            }
            else
            {
                Halogen spareHalogen = new Halogen(0.1);
                _halogen = spareHalogen;
                Communicates.HalogenRenewed();
                _isBroke = false;
            }
        }
    }


}
