﻿using System;

namespace Lamp_Simulator
{
    /// <summary>
    /// Halogen object, renewable part of the lamp, with special failure index
    /// </summary>
    class Halogen
    {
        /// <summary>
        /// When creating new Halogen, its nessessery to give failure index
        /// </summary>
        /// <param name="chance">Failure index</param>
        public Halogen(double chance)
        {
            _chance = chance;
        }
        private bool _isWorking = true;
        public bool IsWorking { get { return _isWorking; } }
        private double _chance;
        static private Random r = new Random();
        /// <summary>
        /// Is halogen burned out during start? Lets figure it out
        /// </summary>
        public bool HalogenBurnedOut()
        {

            if (r.NextDouble() < _chance)
            {

                return _isWorking = false;
            }
            else
            {
                return _isWorking = true;
            }
        }
    }
}
