﻿using System;

namespace Lamp_Simulator
{
    /// <summary>
    /// Class containg all application communicates 
    /// </summary>
    static class Communicates
    {
        public static void SwitchedOn()
        {
            Console.WriteLine("Lamp is switched on");
        }
        public static void HalogenRequired()
        {
            Console.WriteLine("New Halogen Required");
        }
        public static void SwitchedOff()
        {
            Console.WriteLine("Lamp is switched off");
        }
        public static void IsBroken()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Lamp is broken");
        }
        public static void AlreadySwitchedOff()
        {
            Console.WriteLine("Lamp is already switched off");
        }
        public static void AlreadySwitchedOn()
        {
            Console.WriteLine("Lamp is already switched on");
        }
        public static void HalogenRenewed()
        {
            Console.WriteLine("Halogen renewed");
        }
        public static void ReadFunctionCommunicate()
        {
            Console.WriteLine("1 - swich on a lamp");
            Console.WriteLine("2 - swich off a lamp");
            Console.WriteLine("3 - replace halogen");
            Console.WriteLine("4 - finish this wonderfull fun");
        }
        public static void EnterOperator()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Enter Operator: ");
        }
        public static void HalogenOperational()
        {
            Console.WriteLine("Halogen replacement not required");
        }
    }
}
