﻿using System;

namespace Lamp_Simulator
{
    class Operations
    {
        static LampFunction ReadFunction()
        {
            bool isValid = true;
            var signChar = ' ';
            var op = LampFunction.SwitchOn;
            bool isValidForCharTryParse;
            Communicates.EnterOperator();
            do
            {
                isValidForCharTryParse = char.TryParse(Console.ReadLine(), out signChar);
                isValid = true;
                switch (signChar)
                {
                    case '1':
                        op = LampFunction.SwitchOn;
                        break;
                    case '2':
                        op = LampFunction.SwitchOff;
                        break;
                    case '3':
                        op = LampFunction.RenewHalogen;
                        break;
                    case '4':
                        op = LampFunction.Finish;
                        break;
                    default:
                        isValid = false;
                        break;
                }
                if (false == isValid)
                {
                    Console.WriteLine("Try again!");
                }
            } while (false == isValidForCharTryParse || false == isValid);
            return op;
        }
        public void OperateALamp(Lamp deskLamp)
        {
            LampFunction lampFunction;
            Communicates.ReadFunctionCommunicate();
            do
            {
                lampFunction = ReadFunction();
                switch (lampFunction)
                {
                    case LampFunction.SwitchOn:
                        deskLamp.SwitchOn();
                        break;
                    case LampFunction.SwitchOff:
                        deskLamp.SwitchOff();
                        break;
                    case LampFunction.RenewHalogen:
                        deskLamp.replaceHalogen();
                        break;

                }

            } while (lampFunction != LampFunction.Finish);
        }
    }
}
