﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;


namespace Lamp_Simulator
{
    class Program
    {
        static void Main(string[] args)
        {

            Halogen halogenDiall = new Halogen(0.5);
            Lamp deskLamp = new Lamp(halogenDiall);
            Operations LampOperator = new Operations();
            LampOperator.OperateALamp(deskLamp);
            Console.ReadKey();
        }
    }
}
